# variables.tf

# export TF_VAR_hcloud_token="<Hetzner API token>"
variable "hcloud_token" {
  description = "Hetzner Cloud API Token"
  type        = string
  sensitive   = true
}

variable "node_hcloud_token" {
  description = "Hetzner Cloud API Token for workstation"
  type        = string
  sensitive   = true
}

variable "server_type" {
  description = "Server type"
  type        = string
}

variable "network_zone" {
  description = "Server network zone"
  type        = string
}

variable "ssh_authorized_keys" {
  description = "Authorized keys to add for login"
  type        = map(any)
}

variable "location" {
  description = "Server location"
  type        = string
}

variable "hcloud_image_name" {
  description = "Name of install image"
  type        = string
  default     = "alma-9-arm64"
}

variable "ansible_pull_repo" {
  description = "Link to ansible repo to run with ansible-pull"
  type        = string
  default     = "https://github.com/ansible/tower-example"
}

variable "ansible_pull_playbook" {
  description = "Name of playbook to run in ansible repo"
  type        = string
  default     = "helloworld.yml"
}

variable "ansible_pull_checkout" {
  description = "Refspec to checkout in ansible repo"
  type        = string
  default     = "main"
}

variable "ansible_pull_tags" {
  description = "Tags to run in ansible pull"
  type        = string
  default     = "all"
}

variable "workstation_nodes" {
  description = "Workstation node description"
  default     = [{"labels": {"workstation":"client"}}]
}
