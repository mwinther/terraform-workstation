# main.tf

# resource "hcp_vault_secrets_app" "workstation_app" {
#   app_name    = "workstation-${terraform.workspace}"
#   description = timestamp()
# }

provider "hcloud" {
  token = var.hcloud_token
}

data "hcloud_image" "install_image" {
  name     = "${var.hcloud_image_name}"
  with_architecture = "x86"
}

data "hcloud_ssh_key" "authorized_keys" {
  name = keys(var.ssh_authorized_keys)[0]
}

data "template_file" "workstation_cloud_config" {
  template = file("cloudinit/workstation-cloud-config.yaml")

  vars = {
    hcloud_token        = var.node_hcloud_token
    ansible_repo        = var.ansible_pull_repo
    ansible_playbook    = var.ansible_pull_playbook
    ansible_checkout    = var.ansible_pull_checkout
    ansible_tags        = var.ansible_pull_tags
    location            = var.location
    server_type         = var.server_type
  }
}

data "cloudinit_config" "workstation" {
  gzip          = false
  base64_encode = false

  part {
    content_type = "text/cloud-config"
    filename     = "terraform.yaml"
    content      = data.template_file.workstation_cloud_config.rendered
  }
}

resource "random_pet" "workstation" {
  count = 1

  keepers = {
    image             = data.hcloud_image.install_image.id
    host_location     = var.location
    host_type         = var.server_type
    cloudinit_payload = data.cloudinit_config.workstation.rendered
    ssh_keys          = data.hcloud_ssh_key.authorized_keys.id
  }
}

resource "hcloud_server" "workstations" {
  count        = length(var.workstation_nodes)
  name         = random_pet.workstation[count.index].id
  server_type  = random_pet.workstation[count.index].keepers.host_type
  location     = random_pet.workstation[count.index].keepers.host_location
  image        = random_pet.workstation[count.index].keepers.image
  labels       = var.workstation_nodes[count.index]["labels"]
  ssh_keys     = [random_pet.workstation[count.index].keepers.ssh_keys]
  user_data    = random_pet.workstation[count.index].keepers.cloudinit_payload

  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }
}
